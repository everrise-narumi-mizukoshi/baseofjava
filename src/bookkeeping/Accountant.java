package bookkeeping;

public class Accountant extends Employee implements BookkeepingAble{

	public Accountant(String name) {
		super(name);
	}
	
	/**
	 * 経理を起立させる.
	 */
	@Override
	public void standUp() {
		System.out.println("経理の" + getName() + "起立しました");
	}

	@Override
	public void journal() {
		System.out.println(getName() + "は仕分け出来ます");
	}
	

}
