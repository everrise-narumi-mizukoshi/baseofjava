package bookkeeping;

public class President extends Employee implements BookkeepingAble{

	public President(String name) {
		super(name);
	}

	@Override
	public void journal() {
		System.out.println(getName() + "は仕分けできません");
	}
	

}
