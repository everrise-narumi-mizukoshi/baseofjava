package bookkeeping;

public class General extends Employee implements BookkeepingAble{

	public General(String name) {
		super(name);
	}
	
	/**
	 * 総務起立させる.
	 */
	@Override
	public void standUp() {
		System.out.println("総務の" + getName() + "起立しました");
	}

	@Override
	public void journal() {
		System.out.println(getName() + "は仕分け出来ます");
		
	}

}
