package bookkeeping;

import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {
		// 総務, 経理, 社長に仕分けさせる
		ArrayList<BookkeepingAble> staffList = new ArrayList<BookkeepingAble>();
		staffList.add(new General("総務１"));
		staffList.add(new Accountant("経理１"));
		staffList.add(new President("社長"));
		
		// 総務、経理、社長はみんな簿記の資格持ってるから仕分けをよびだせる
		// けど、総務と経理は仕分けの中身が違うから、できない社長みたいな人が出てくる
		for (BookkeepingAble staff : staffList) {
			staff.journal();
		}
		
		

	}

}
