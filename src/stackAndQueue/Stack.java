package stackAndQueue;

import java.util.Arrays;

public class Stack {
    private static int stackSize = 100;
    private static String[] stack = new String[stackSize];
    private static int stackPointer = 0;

    public static void main(String[] args) {
        
        // 101回プッシュする : 101回目はエラーになる
        for (int i = 0; i <= 100; i++) {
            stackPush(String.valueOf(i));
        }
        // データが100個入ってることを確認
        System.out.println(Arrays.toString(stack));
        
        // データを取り出して表示（stackPopを100回呼ぶ）
        for (int i = 0; i < 100; i++) {
            System.out.println(stackPop());
        }
    }
    
    public static void stackPush(String val) {
        if (stackPointer >= stackSize) {
            System.out.println("スタックの容量がいっぱいです");
            return;
        }
        stack[stackPointer++] = val;
    }
    
    public static String stackPop() {
        if (stackPointer <= 0) {
            System.out.println("スタックが空です");
            return null;
        }
        String val = stack[--stackPointer];
        stack[stackPointer] = null;
        return val;
    }

}
