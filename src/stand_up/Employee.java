package stand_up;

public class Employee {
	/** 社員名 */
	private String name;
	
	/**
	 * コンストラクタ.
	 * @param name 社員名
	 */
	public Employee(String name) {
		this.name = name;
	}
	
	/**
	 * 社員を起立させる.
	 */
	public void standUp() {
		System.out.println(getName() + "起立しました");
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
