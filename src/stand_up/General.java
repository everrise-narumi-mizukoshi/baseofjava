package stand_up;

public class General extends Employee{

	public General(String name) {
		super(name);
	}
	
	/**
	 * 総務起立させる.
	 */
	@Override
	public void standUp() {
		System.out.println("総務の" + getName() + "起立しました");
	}

}
