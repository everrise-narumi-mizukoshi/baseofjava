package stand_up;

public class Accountant extends Employee{

	public Accountant(String name) {
		super(name);
	}
	
	/**
	 * 経理を起立させる.
	 */
	@Override
	public void standUp() {
		System.out.println("経理の" + getName() + "起立しました");
	}
	

}
