package stand_up;

public class Manager extends Employee{
	public Manager(String name) {
		super(name);
	}
	/**
	 * 部長を起立させる.
	 */
	@Override
	public void standUp() {
		System.out.println("部長の" + getName() + "起立しました");
	}
	

}
