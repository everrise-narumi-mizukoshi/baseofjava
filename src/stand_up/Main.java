package stand_up;

import java.util.ArrayList;

public class Main {

	public static void main(String[] args) {
		// 社員起立
		Employee employee = new Employee("hoge山hoge太郎");
		employee.standUp();
		
		// 部長起立
		Manager manager = new Manager("hoge川hoge次郎");
		manager.standUp();
		
		// 総務起立
		General general = new General("hoge田hoge三郎");
		general.standUp();
		
		// 経理起立
		Accountant accountant = new Accountant("hoge海hoge四郎");
		accountant.standUp();
		
		// 部長2人, 総務10人, 経理5人を起立させる
		// スタッフリスト作成
		ArrayList<Employee> staffList = new ArrayList<>();
		staffList.add(new Manager("部長１"));
		staffList.add(new Manager("部長２"));
		staffList.add(new General("総務１"));
		staffList.add(new General("総務２"));
		staffList.add(new General("総務３"));
		staffList.add(new General("総務４"));
		staffList.add(new General("総務５"));
		staffList.add(new General("総務６"));
		staffList.add(new General("総務７"));
		staffList.add(new General("総務８"));
		staffList.add(new General("総務９"));
		staffList.add(new General("総務１０"));
		staffList.add(new Accountant("経理１"));
		staffList.add(new Accountant("経理２"));
		staffList.add(new Accountant("経理３"));
		staffList.add(new Accountant("経理４"));
		staffList.add(new Accountant("経理５"));
		
		// 起立させる
		for (Employee staff : staffList) {
			staff.standUp();
		}
		
		

	}

}
